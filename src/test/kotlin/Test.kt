package Nexa.NexaRpc

import java.math.BigDecimal
import kotlin.test.Test
import kotlin.test.*

class NexaRpcTest
{
    @Test
    fun basicReg()
    {
        println("This test requires that a nexa regtest network be running on the localhost at port 18332")
        val rpc = NexaRpcFactory.create("http://127.0.0.1:18332")
        val unspent = rpc.listunspent()
        assertTrue {unspent.size > 0 }
        assertEquals(1 ,1)

        val bal = rpc.getbalance()
        assertTrue {bal > BigDecimal.ZERO }

        val peers = rpc.getpeerinfo()
        assertTrue{peers.size < 2}  // Probably true anyway

        val addr = rpc.getnewaddress()
        println(addr)
        val addr2 = rpc.getnewaddress("p2pkh")
        println(addr2)
        assertTrue { addr.length > addr2.length}  // just a property of the 2 address formats that helps confirm that the right ones were created
        val tx = rpc.sendtoaddress(addr, BigDecimal("1000.23"))

        val txpool = rpc.getrawtxpool()
        assertEquals(txpool.size, 1)

        val txpoolinfo = rpc.gettxpoolinfo()
        assertEquals(txpoolinfo.size, 1)
        assertTrue { txpoolinfo.bytes > 100}

        val rawTx = rpc.getrawtransaction(txpool[0].toHex())
        assertTrue { rawTx.size > 100 }

        val blkhashes = rpc.generate(1)
        assertEquals(blkhashes.size, 1)

    }
}
