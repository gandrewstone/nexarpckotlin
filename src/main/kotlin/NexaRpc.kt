package Nexa.NexaRpc

import kotlinx.serialization.*
import kotlinx.serialization.json.JsonElement
import java.math.BigDecimal

class NexaRpcException(msg:String, val code: Long):Exception(msg)

private val HEX_CHARS = "0123456789abcdef"
// Convert a hex string to a ByteArray
fun String.fromHex(): ByteArray
{

    val result = ByteArray(length / 2)

    for (i in 0 until length step 2)
    {
        val firstIndex = HEX_CHARS.indexOf(this[i]);
        val secondIndex = HEX_CHARS.indexOf(this[i + 1]);

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    return result
}

fun Byte.toUint():Int = toInt() and 0xFF

fun ByteArray.toHex(): String
{
    val ret = StringBuilder()
    for (b in this)
    {
        ret.append(HEX_CHARS[b.toUint() shr 4])
        ret.append(HEX_CHARS[b.toUint() and 0xf])
    }
    return ret.toString()
}

class HashId(val hash: ByteArray = ByteArray(32, { _ -> 0 }))
{
    constructor(hex: String) : this()
    {
        val hsh = hex.fromHex()
        hsh.reverse()
        hsh.copyInto(hash)
    }

    /** Convert to the bitcoin standard hex representation */
    fun toHex(): String
    {
        val cpy = hash.copyOf()
        cpy.reverse()
        return cpy.toHex()
    }

    /** The default display will be bitcoin standard hex representation (reversed hex) */
    override fun toString(): String = this.toHex()

    override fun equals(other: Any?): Boolean
    {
        if (other is HashId) return hash contentEquals other.hash
        return false
    }

}

interface NexaRpc
{
    @Serializable
    data class RpcError(val code:Long, val message: String)

    @Serializable
    // Do not use the amount field because it is not a decimal fraction.  Use the satoshis field.
    data class Unspent(val outpoint: String, val txid: String, val txidem: String, val vout: Long, val address:String, val scriptPubKey: String, val scriptType: String, val satoshi: Long, val amount: Double, val confirmations: Long, val spendable: Boolean)
    @Serializable
    data class ListUnspentReply(val result: List<Unspent>?, val error: RpcError?)

    @Serializable
    data class GetBalanceReply(val result: Double?, val error: RpcError?)  // TODO change to BigDecimal when supported by Kotlin

    @Serializable
    data class WalletInfo(val walletversion: Long, val syncblock: String, val syncheight:Long, val Balance: Double, val unconfirmed_balance: Double, val immature_balance: Double, val txcount: Long, val keypoololdest: Long, val keypoolsize:Long, val paytxfee: Long, val hdmasterkeyid:String)
    @Serializable
    data class GetWalletInfoReply(val result: WalletInfo?, val error: RpcError?)

    @Serializable
    data class GetNewAddressReply(val result: String?, val error: RpcError?)

    @Serializable
    data class TxPoolInfo(val size: Long, val bytes: Long, val usage: Long, val maxtxpool: Long, val txpoolminfee: Double, val tps: Double, val peak_tps: Double)
    @Serializable
    data class TxPoolInfoReply (val result: TxPoolInfo?, val error: RpcError?)

    @Serializable
    data class PeerInfo(val id:Long, val addr: String, val addrlocal: String, val services: String, val servicesnames: List<String>, val relaytxes: Boolean, val lastsend: Long, val lastrecv: Long,
                        val bytessent: Long, val bytesrecv:Long, val conntime: Long, val timeoffset: Long, val pingtime:Double, val minping: Double, val version: Long, val subver: String, val inbound: Boolean,
                        val startingheight: Long, val banscore: Long, val synced_headers: Long, val synced_blocks: Long, val inflight: List<String>, val whitelisted: Boolean, val extversion_map: Map<String, String>)
    @Serializable
    data class GetPeerInfoReply(val result: List<PeerInfo>, val error: RpcError?)

    //@Serializable
    //data class Reply<T> (val result: T?, val error: RpcError?)

    @Serializable
    data class HexHashReply(val result: String?, val error: RpcError?)
    @Serializable
    data class HexHashListReply(val result: List<String>?, val error: RpcError?)

    val url:String?
    val user:String?
    val pwd:String?

    fun callje(rpcName: String, params:List<String>?=null): JsonElement
    fun calls(rpcName: String, params:List<String>?=null): String

    fun listunspent(): List<Unspent>
    fun generate(qty: Int):List<HashId>
    fun getrawtxpool():List<HashId>
    fun gettxpoolinfo(): TxPoolInfo
    fun sendtoaddress(addr: String, amt: BigDecimal):HashId
    fun getbalance(): BigDecimal
    fun getwalletinfo(): WalletInfo
    fun getpeerinfo(): List<PeerInfo>
    fun getnewaddress(addrType:String? = null): String

    fun getrawtransaction(hash:String): ByteArray
    fun getrawtransaction(hash:HashId): ByteArray
}

/*
expect object NexaRpcFactory
{
    fun create(url:String="http://10.0.2.2:18332/", user:String ="regtest", pwd:String = "regtest"): NexaRpc
}

 */


